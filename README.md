# An unofficial Yamaha MusicCast Command Line Client

MusicCast is Yamaha's streaming platform built in their Hi-Fi devices.
MusicCast devices speak an API used by the MusicCast app. ymcl is a
command-line utility that can be used to remote-control and read the
status of MusicCast devices in the local network. ymcl is not a
substitute for the MusicCast app: it is limited to controlling a device
which has already been set up with the MusicCast app.

ymcl's non-exhaustive list of supported actions:

- switching on/off,
- setting volume/mute,
- selecting an input to play from,
- selecting a tuner or netradio preset,
- playback controls: stop/play/previous/next track, fast forward,
  rewind, whenever "track" makes sense (e.g. CD),
- compact status of what is the current input/volume/power,
- view currently playing track,
- "link" devices: make the one use the other as input.

In addition, virtually any API endpoint can be addressed by using the
raw GET and POST actions.

# Usage

	% ymcl <action> [<parameters>]

where `<action>` and `<parameters>` are as shown in the `case..esac` below.

## examples

	% export YMC_HOST=hifione
	% ymcl on
	% ymcl volume up 		# increase volume of device hifione
	% ymcl volume 34
	% ymcl radio 1
	% YMC_HOST=box ymc radio 2	# play second radio preset on device box
	% ymcl -y box radio 2		# equivalent to the previous line

## actions

	v*)		# volume up/down/abs.value
	on|standby|toggle)
	off)		# alias for standby
	net|netr|netradio)	# choosing net_radio preset or list presets (without parameter)
	r|radio|tuner)	# choosing fm radio preset or list presets (without parameter)
	rds)	# playing info for tuned station
	in|input)	# chose input
	bt)		# synonym for input bluetooth
	cd|CD)		# synonym for input cd
	play)		# playback controls:
			stop|play|previous|next)
			fr)			# fast rewind start
			frstop)			# fast rewind stop
			ff)			# fast forward start
			ffstop)			# fast forward stop
	stat|stats|status) # customized summary of device's status
	song|info|playing) # info for current playing song/station
	GET|api)		# request to api url using GET and pretty print (jq)
	POST|apiPOST)		# request to api url using POST and supplied payload; and pretty print (jq)
	link)		# link the first device to to be source for the second device
	ip)		# device's IP address
	overview)	# print status for each device configured in ~/.ymc
	-y|--yamaha-host)	# host name or IP
	-h|--help)	# this message

# links

- https://forums.indigodomo.com/viewtopic.php?t=18367
- https://musiccast2mqtt.readthedocs.io/en/latest/musiccast_usage.html
- http://$host/YamahaExtendedControl/v1/system/getFeatures

# Notes

## Linking music cast devices

(aka distribution)

cf. Section 9 in  ./Yamaha_Extended_Control_API_Specification_%28Advanced%29.pdf (ElVis wiki)

check compatibility

	% YMC_HOST=server ymc api 'system/getFeatures' | jq .distribution.compatible_client
	[
	  2
	]
	% YMC_HOST=client ymc api 'system/getFeatures' | jq .distribution.version
	2

