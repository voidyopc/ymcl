#!/bin/dash

we=`basename $0`

# from the config we expect the vars hosts and default_host
read_conf() {
	sed '/^\s*#/ d; /^\s*$/ d;' <~/.ymc
}
eval `read_conf`

# command line switch -y takes precedence before environment variable
# YMC_HOST takes precedence before config file `default_host`.
host=${YMC_HOST:-default_host}

bad_param () {
	echo $@
}

usage () {
	echo $@
	cat <<.
Usage:
	% $we <action> [<params>]
where <action> and <parameters> are as shown in the case..esac below.

examples:
	% ymcl volume up
	% ymcl volume 34
	% ymcl on
	% yncl toggle
	% ymcl off
	% ymcl radio 1
actions:
.
	sed -n '/^case/,/^esac/{ /^[^()]*)/ p}' $0
	echo "(note: using host '$host')"
	exit 0
}

die() {
	[ -n "$2" ] && 1>&2 echo we: $2
	echo $1
	exit 1
}



cmd() {
	curl -s http://$host/YamahaExtendedControl/v1/"$1"
}

cmd_safe() {
	res=`cmd "$1" | jq .response_code`
	[ "x$res" = "x0" ] || die $res "$host response code $res"
}

post() {
	curl -s -X POST http://$host/YamahaExtendedControl/v1/"$1" -d "$2"
}

ip_of() {
	h=${1:-$host}
	host=$h cmd system/getNetworkStatus | jq -r '.ip_address'
}

link() {
	server=`ip_of $1`
	client=`ip_of $2`

	server_info_remove_client_json='{"group_id": "b406fddd733c4222ac2f16be98762d19", "zone":"main", "type":"remove", "client_list":["'$client'"] }'
	server_info_add_client_json='{"group_id": "b406fddd733c4222ac2f16be98762d19", "zone":"main", "type":"add", "client_list":["'$client'"] }'
	client_info_set_group_json='{"group_id": "b406fddd733c4222ac2f16be98762d19", "zone":"main"}'
	client_info_remove_group_json='{"group_id": "", "zone":"main"}'

	echo "removing group from client..."
	host=$client	post	dist/setClientInfo "$client_info_remove_group_json"
	echo "removing client from server..."
	host=$server	post	dist/setServerInfo "$server_info_remove_client_json"
	sleep .2
	echo "adding client to server..."
	host=$server	post	dist/setServerInfo "$server_info_add_client_json"
	echo "setting group to client..."
	host=$client	post	dist/setClientInfo "$client_info_set_group_json"
	sleep .2
	echo starting distribution...
	host=$server	cmd	'dist/startDistribution?num=0'
	echo done.
	host=$client	cmd	'dist/getDistributionInfo'
	host=$server	cmd	'dist/getDistributionInfo'
	echo $server $client
	host=$client status
	host=$server status
}

status() {
	loc=`cmd system/getLocationInfo | jq -r '.name'`
	net=`cmd system/getNetworkStatus | jq -r ".network_name"`
	stats=`cmd main/getStatus | jq -r ".power, .volume, .max_volume, .mute, .input"`
	echo $stats | (read power vol max muted input;
	# echo "$loc · $net · $power · $input · $vol/$max  · $mute")
	muted=`[ x"$mute" = x"false" ] && echo muted || echo unmuted`
	playing=`[ x"$power" = x"on" ] && echo plays || echo would play`
	echo "$loc·$net is $power, $playing from $input $muted at $vol/$max")
}

song() {

	input=`cmd main/getStatus | jq -r ".input"`
	jq_filter='
			# (.track_number|tostring) +  "/" + (.total_tracks|tostring) + " " + (.track|tostring),
			# (if .track_number then . else "_" end) +  "/" + (if .total_tracks then . else "_" end) + " " + (.track),
			# write track number / all unless track_number is null (spotify)
			if .track_number then
				(.track_number|tostring +  "/" + .total_tracks|tostring + " ")
			else
				""
			end + (.track),
			"from " + (.album|tostring),
			"by " + (.artist|tostring),
			# write play time unless it is stub (spotify)
			if .play_time != -60000 then (.play_time|tostring + "s") else "" end
			'
	case "$input" in
		net_radio|spotify|server) cmd netusb/getPlayInfo | jq -r "$jq_filter" ;;
		cd) cmd cd/getPlayInfo | jq -r "$jq_filter" ;;
		tuner) cmd tuner/getPlayInfo | jq -r '
			.rds.program_service,
			.rds.radio_text_a,
			.rds.radio_text_b'
			;;
		*) bad_param "$input is not supported for song info"
	esac
}

case "$1" in
	v*)		# volume up/down/abs.value
		cmd_safe "main/setVolume?volume=$2"
		;;
	mute)		# mute true/false/nothing
		cmd_safe "main/setMute?enable=${2:-true}"
		;;
	unmute)		# =mute false
		cmd_safe "main/setMute?enable=false"
		;;
	on|standby|toggle)
		cmd_safe "main/setPower?power=$1"
		;;
	off)		# alias for standby
		cmd_safe "main/setPower?power=standby"
		;;
	net|netr|netradio)	# choosing net_radio preset or list presets (without parameter)
		[ -n "$2" ] \
			&& cmd_safe "netusb/recallPreset?zone=main&num=$2" \
			|| (cmd "netusb/getPresetInfo" | jq '.preset_info[].text' | cat -n | sed '/""/ d')
		;;
	r|radio|tuner)	# chosing fm radio preset or list presets (without parameter)
		[ -n "$2" ] \
			&& cmd_safe "tuner/recallPreset?zone=main&band=fm&num=$2" \
			|| (cmd "tuner/getPresetInfo?band=fm" | jq '.preset_info[].number / 1000' | cat -n | sed '/\t0$/ d')
		;;
	rds)	# playing info for tuned station
		cmd "tuner/getPlayInfo" \
			| jq -r '.fm.freq, .rds.program_service, .rds.radio_text_a, .rds.radio_text_b' \
			| sed 's/^ *//g'
		;;
	in|input)	# chose input
		[ -n "$2" ] \
			&& cmd_safe "main/setInput?input=$2" \
			|| usage "input needs a parameter"
		;;
	bt)		# synonym for input bluetooth
		$0 input bluetooth
		;;
	cd|CD)		# synonym for input cd
		$0 input cd
		;;
	play)		# play, stop, next, etc
		input=`cmd main/getStatus | jq -r ".input"`
		case $2 in
			stop|play|previous|next) # pla
				cmd_safe $input/setPlayback?playback=$2
				;;
			fr) # fast rewind start
				cmd_safe $input/setPlayback?playback=fast_reverse_start
				;;
			frstop) # fast rewind stop
				cmd_safe $input/setPlayback?playback=fast_reverse_end
				;;
			ff) # fast forward start
				cmd_safe $input/setPlayback?playback=fast_forward_start
				;;
			ffstop) # fast forward stop
				cmd_safe $input/setPlayback?playback=fast_forward_end
				;;
		esac
		;;
	stat|stats|status)# customized summary of device's status
		status
		;;
	song|info|playing)# info for current playing song/station
		song
		;;
	GET|api)		# request to api url using GET and pretty print (jq)
		[ -n "$2" ] \
			&&  cmd "$2" | jq . \
			|| usage "api needs a parameter"
		;;
	POST|apiPOST)		# request to api url using POST and supplied payload; and pretty print (jq)
		[ -n "$3" ] \
			&& post "$2" "$3" | jq . \
			|| usage "apiPOST needs 2 parameters"
		;;
	link) # link the first device to to be source for the second device
		link "$2" "$3"
		;;
	ip) # device's IP address
		ip_of $host
		;;
	name) # device's name
		cmd system/getNetworkStatus | jq -r ".network_name"
		;;
	overview) # print status for each device configured in ~/.ymc
		eval `read_conf`
		for host in $hosts; do status; done
		;;
	-y|--yamaha-host)	# use this host or IP
		host=$2
		if [ "$host" = "all" ]; then
			shift
			shift
			eval `read_conf`
			for host in $hosts; do $we -y $host $1; done
		else
			shift
			shift
			YMC_HOST=$host $we $@
		fi
		;;
	-h|--help)	# this message
		usage
		;;
	*)		# everything else
		echo "Unsupported command: " $@
		echo "use -h for help"
esac
